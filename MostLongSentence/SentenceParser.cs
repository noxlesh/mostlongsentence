﻿using System;
using System.Collections.Generic;

namespace MostLongSentence
{
    public class SentenceParser
    {
        private readonly string _text;
        private readonly List<string> _sentences;
        private int _sentenceStart;
        private int _sentenceEnd;
        
        public SentenceParser(string text)
        {
            _text = text;
            _sentences = new List<string>();
            ParseToSentences();
            
        }

        private void ParseToSentences()
        {
            for (int i = 0; i < _text.Length; i++)
            {
                char t = _text[i];
                if (t == '.' || t == '?' || t == '!')
                {
                    _sentenceEnd = i + 1 - _sentenceStart;
                    SingleSentence();
                    _sentenceStart = i + 1;
                }
            }
        }

        private void SingleSentence()
        {
            String s = _text.Substring(_sentenceStart, _sentenceEnd);
            //Console.WriteLine(s);
            _sentences.Add(s);
        }

        public string MostLongSentence()
        {
            int longestSentenceIndex = 0;
            int prevWordcount = 0;
            for (int sIndex = 0; sIndex < _sentences.Count; sIndex++)
            {
                char prevSymbol = '\0';
                int wordCount = 0;
                string sentence = _sentences[sIndex];
                for (int charIndex = 0; charIndex < sentence.Length; charIndex++)
                {
                    char t = sentence[charIndex];
                    if ((t == ' ' || t == ',' || t == '.' || t == '!' || t == '?') && charIndex != 0)
                    {
                        if (prevSymbol != ' ' && prevSymbol != ',')
                        {
                            wordCount++;
                        }
                        
                    }
                    prevSymbol = t;
                }

                if (wordCount > prevWordcount)
                    longestSentenceIndex = sIndex;
                prevWordcount = wordCount;
            }   
            return _sentences[longestSentenceIndex];
        }
    }
}