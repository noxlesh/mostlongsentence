﻿using System;

namespace MostLongSentence
{
    internal class Program
    {
        // Sample text
        private static string  txt = "Loop, over string chars. "+
        "A loop iterates,  over individual,  string characters! "+
        "It allows processing, for each char."+
        " The foreach-loop and,  the   for-loop are available for this purpose?"+
        " The string indexer returns a char value.";
        
        public static void Main(string[] args)
        {
            SentenceParser sp = new SentenceParser(txt);
            Console.WriteLine(sp.MostLongSentence());
        }
    }
}